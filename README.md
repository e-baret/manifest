# stm32mp1 manifest
This manifest is made to build images for the STM32MP157F-DK2 board using [ELF](https://gitlab.com/embedded_linux_factory/elf).

## Dependencies 

- [poky](https://github.com/yoctoproject/poky) - layer : meta
- [meta-openembedded](https://github.com/openembedded/meta-openembedded) - layers : meta-python meta-oe
- [meta-st-stm32mp1](https://github.com/STMicroelectronics/meta-st-stm32mp)
- [meta-tns-stm](https://gitlab.com/e-baret/meta-tns-stm)

## Firmwares
This manifest is only used to build Linux distribution and install firmwares on it.

Instructions for compiling a firmware and flashing it are available in the repository [stm32mp1-fw](https://gitlab.com/e-baret/stm32mp1-fw). It's also include some firmwares examples.

# How to build images

## Set environnment 

First you must be in an empty folder.

Next install [ELF](https://gitlab.com/embedded_linux_factory/elf) tool to use this manifest.
~~~shell
$ git clone https://gitlab.com/embedded_linux_factory/elf.git
~~~
And clone this repo :
~~~shell
$ git clone https://gitlab.com/e-baret/manifest.git
~~~~

Finally create a project folder :
~~~shell
$ mkdir project-stm32mp1
~~~

You should now have the following folder tree :
~~~
.
├── elf                 [clone of ELF repository]
├── manifest            [clone of this repository]
└── project-stm32mp1    [empty project folder]
~~~

## Create images

Start ELF, for the first use launch ELF with -m option :
~~~shell
# move to project folder
$ cd ./project-stm32mp1
# launch elf
$ ../elf/elf.py
# launch elf with manifest for the first use
$ ../elf/elf.py -m ../manifest/stm32mp1.xml
~~~

Next, compile the linux distribution with bitbake :
~~~shell
$ bitbake < image >
~~~
Images available can be found [here](https://gitlab.com/e-baret/meta-tns-stm/-/tree/dunfell/recipes-tns/images).


To create an image, change your current directory to "< workspace >/build/tmp/deploy/images/stm32mp1".

And launch the following script :
~~~shell
$ ./scripts/create_sdcard_from_flashlayout.sh ./flashlayout_<your image>/trusted/FlashLayout_sdcard_stm32mp157f-dk2-trusted.tsv 
~~~

Image will be created with the following name in the current directory :
FlashLayout_sdcard_stm32mp157f-dk2-trusted.raw

To finish, flash image on SD card with [balenaEtcher](https://www.balena.io/etcher/)

